terraform {
  backend "s3" {
    region  = "us-east-1"
    profile = "default"
    key     = "terraform-state"
    bucket  = "terraform-state-bucket-jknsproj"
  }
}
