data "aws_ssm_parameter" "linuxAmi" {
   provider = aws.master
   name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

#key-pair for master
resource "aws_key_pair" "master-key" {
   provider = aws.master
   key_name = "jenkins"
   public_key = file("~/.ssh/id_rsa.pub")
}

#key-apir for worker
resource "aws_key_pair" "worker-key" {
   provider = aws.master
   key_name = "jenkins_worker"
   public_key = file("~/.ssh/id_rsa.pub")
}

#EC2 instance for the jenkins master
resource "aws_instance" "jenkins-master" {
   provider = aws.master
   ami = data.aws_ssm_parameter.linuxAmi.value
   instance_type = var.instance-type
   key_name = aws_key_pair.master-key.key_name
   associate_public_ip_address = true
   vpc_security_group_ids = [aws_security_group.jenkins-sg.id]
   subnet_id = aws_subnet.subnet_1.id
   
   tags = {
      Name = "jenkins_master_tf"
   }

   depends_on = [aws_main_route_table_association.set-master-default-rt-assoc]

   provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.region} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name}' ansible_templates/install_jenkins_master.yml
EOF
  }
}

#create worker EC2
resource "aws_instance" "jenkins-worker" {
  provider = aws.master
  count = var.workers-count
  ami  = data.aws_ssm_parameter.linuxAmi.value
  instance_type = var.instance-type
  key_name = aws_key_pair.worker-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.jenkins-sg-worker.id]
  subnet_id = aws_subnet.subnet_1_oregon.id

  tags = {
    Name = join("_", ["jenkins_worker_tf", count.index + 1])
  }
  depends_on = [aws_main_route_table_association.set-worker-default-rt-assoc, aws_instance.jenkins-master]

  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.region} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name} master_ip=${aws_instance.jenkins-master.private_ip}' ansible_templates/install_jenkins_worker.yml
EOF
  }
   
}
