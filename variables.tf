variable "profile" {
   type = string
   default = "default"
}

variable "region" {
   type = string
   default = "us-east-1"
}

variable "external_ip" {
   type = string
   default = "0.0.0.0/0"
}

variable "workers-count" {
   type = number
   default = 1
}


variable "instance-type" {
   type = string
   default = "t2.micro"
}

variable "webserver-port" {
   type = number
   default = 8080
}

variable "dns-name" {
   type = string
   default = "cmcloudlab741.info."
} 
